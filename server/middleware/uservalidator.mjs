import { object, string} from 'yup';
import raw from '../middleware/route.async.wrapper.mjs';

let userSchema = object({
  first_name: string().required().max(20).min(2),
  last_name: string().required().max(20).min(2),
  email: string().email(),
  phone: string().matches(new RegExp('^[0-9]{3}[-\s\.][0-9]{3}[-\s\.][0-9]{4}$'))
});


export const validateUser =  raw(async (req,res,next)=>{
    const user=req.body
    await userSchema.validate(user)
    next()
})